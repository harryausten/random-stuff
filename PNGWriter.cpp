#include <iostream>

#include "PNGWriter.hpp"

PNGWriter::PNGWriter(uint32_t H, uint32_t W, const std::string & filename)
: properties{W,H}
{
    file.open(filename, std::ios::in | std::ios::out | std::ios::binary | std::ios::trunc);
    ihdr();
    idat_begin();
}

PNGWriter::~PNGWriter()
{
    idat_end();
    iend();
    file.close();
}

void PNGWriter::ihdr()
{
    uint8_t png_signature[] = {137,80,78,71,13,10,26,10};
    file.write(reinterpret_cast<char*>(&png_signature),8);

    uint32_t num_bytes = sizeof(properties);
    file.write(reinterpret_cast<char*>(&num_bytes),4);

    char ihdr[] = "IHDR";
    file.write(reinterpret_cast<char*>(&ihdr),4);

    file.write(reinterpret_cast<char*>(&properties),num_bytes);

    crc(num_bytes);

    std::cout << "End of IHDR: " << file.tellp() << std::endl;
}

void PNGWriter::idat_begin()
{
    // Placeholder
    uint32_t num_bytes = 0;
    file.write(reinterpret_cast<char*>(&num_bytes),4);

    char idat[] = "IDAT";
    file.write(reinterpret_cast<char*>(&idat),4);
}

void PNGWriter::write(Colour & x)
{
    data_bytes.push_back(x.r);
    data_bytes.push_back(x.g);
    data_bytes.push_back(x.b);
}

void PNGWriter::idat_end()
{
    std::cout << "IDAT END" << std::endl;
    uint8_t zero = 0;
    for (size_t x = 0; x < data_bytes.size(); x++)
        file.write(reinterpret_cast<char*>(&zero),1);
    crc(data_bytes.size());
    file.seekp(33,std::ios::beg);
    uint32_t num_bytes = data_bytes.size();
    file.write(reinterpret_cast<char*>(&num_bytes),4);
    file.seekp(0,std::ios::end);
}

void PNGWriter::iend()
{
    uint32_t num_bytes = 0;
    file.write(reinterpret_cast<char*>(&num_bytes),4);

    char iend[] = "IEND";
    file.write(reinterpret_cast<char*>(&iend),4);

    crc(num_bytes);

    std::cout << "End of IEND: " << file.tellp() << std::endl;
}

void PNGWriter::crc(size_t num_bytes)
{
    int buf_length = num_bytes + 4;
    char * buf = new char [buf_length];
    file.seekg(-buf_length,std::ios::end);
    file.read(buf,buf_length);
    uint32_t c = update_crc(0xffffffffL, buf, buf_length) ^ 0xffffffffL;
    file.write(reinterpret_cast<char*>(&c),4);
}

// Update a running CRC with the bytes buf[0..len-1]--the CRC should be initialized
// to all 1's, and the transmitted value is the 1's complement of the final running
// CRC (see the crc() routine below)
uint32_t PNGWriter::update_crc(uint32_t crc, char * buf, int len)
{
    uint32_t c = crc;
    int n;

    if (!crc_table_computed)
        make_crc_table();
    for (n = 0; n < len; n++)
        c = crc_table[(c ^ buf[n]) & 0xff] ^ (c >> 8);
    return c;
}

// Make the table for a fast CRC
void PNGWriter::make_crc_table()
{
    uint32_t c;
    int n, k;

    for (n = 0; n < 256; n++) {
        c = static_cast<uint32_t>(n);
        for (k = 0; k < 8; k++) {
            if (c & 1)
                c = 0xedb88320L ^ (c >> 1);
            else
                c = c >> 1;
        }
        crc_table[n] = c;
    }
    crc_table_computed = true;
}
