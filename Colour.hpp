#ifndef __COLOUR_HPP__
#define __COLOUR_HPP__

#include <cstdint>

struct Colour
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
};

#endif // __COLOUR_HPP__
