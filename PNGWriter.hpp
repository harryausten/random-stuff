#ifndef __PNGWRITER_HPP__
#define __PNGWRITER_HPP__

#include <fstream>
#include <vector>

#include "Colour.hpp"

class PNGWriter
{
private:
    std::fstream file = {};
    // Table of CRCs of all 8-bit messages
    uint32_t crc_table[256];
    // Flag: has the table been computed? Initially false
    bool crc_table_computed = false;
    void make_crc_table();
    uint32_t update_crc(uint32_t crc, char * buf, int len);
    uint32_t crc(char * buf, int len);

    std::vector<uint8_t> data_bytes = {};

    struct prop {
        uint32_t width;
        uint32_t height;
        uint8_t bit_depth = 8;
        uint8_t colour_type = 2;
        uint8_t compression_type = 0;
        uint8_t filter_type = 0;
        uint8_t interlace_type = 0;
    } __attribute__((packed)) properties;
public:
    PNGWriter(uint32_t H, uint32_t W, const std::string & filename);
    ~PNGWriter();
    void ihdr();
    void idat_begin();
    void write(Colour & x);
    void idat_end();
    void iend();
    void crc(size_t buf_length);
};

#endif // __PNGWRITER_HPP__
